#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "bkdb.h"

int main(int argc, char *argv[])
{
        bkdb* db;
        int i;

        if (!(db = (bkdb *)malloc(sizeof(bkdb)))) {
                db->db_flag = 0;
                printf("Malloc error\n");
                exit(-1);
        }
        printf("ok m\n");

        for (i = 0; i < 100; i++) {
                if (!(db = (bkdb *)realloc(db, sizeof(bkdb) * (i + 2)))) {
                        printf("Realloc error\n");
                        exit(-2);
                }
                printf("ok r\n");
                (*(db + i + 1)).db_flag = i;

        }

        for (i = 0; i < 101; i++) {
                printf("%d\n", (*(db + i)).db_flag);
        }
        return 0;
}

