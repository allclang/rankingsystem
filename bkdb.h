#ifndef _BKDB_H_
#define _BKDB_H_

#include <db.h>
#include <stdio.h>
#include <stddef.h>

typedef DB_ENV* bkdb_env;
typedef DB* bkdb_db;

typedef struct
{
        bkdb_env db_env;
        bkdb_db db;
        u_int32_t env_flag;
        u_int32_t db_flag;
        char* db_name;
        char* env_path;
} bkdb;

int bkdb_init(bkdb_env*, const char* env_path, u_int32_t env_flag,
              const char *db_name, u_int32_t db_flag);

#endif
