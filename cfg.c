#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "cfg.h"
#include "base.h"

void init_config(struct cfg *gconfig)
{
        if (!(gconfig = (struct cfg *)malloc(sizeof(struct cfg)))) {
                fprintf(stderr, "Can not alloc memory for config\n");
                exit(E_MEM);
        }

        gconfig->maxcon = 65536;
        gconfig->maxtask = 8192;
        gconfig->port = 10027;
        gconfig->ip = "0.0.0.0";
}
