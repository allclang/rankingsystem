#ifndef _dyn_ARRAY_H
#define _dyn_ARRAY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct
{
        int size;
        int alloced;
        int cur_index;
        void* data;
} _array;

int array_init(struct _array **, int size);
int array_free(dyn_array **);
int array_append_elm(void *);
int array_del_elm(int index);
int array_pop(dyn_array **);

#endif
