#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <stddef.h>
#include <fcntl.h>
#include <pwd.h>

#ifndef HAVE_SIGIGNORE
int sigignore(int sig)
{
        struct sigaction sa = { .sa_handler = SIG_IGN, .sa_flags = 0 };

        if (sigemptyset(&sa.sa_mask) == -1 || sigaction(sig, &sa, 0) == -1) {
                return -1;
        }
        return 0;
}
#endif

int daemonize(int nochdir, int noclose)
{
        int fd;

        switch (fork()) {
                case -1:
                        return (-1);
                case 0:
                        break;
                default:
                        _exit(EXIT_SUCCESS);
        }

        if (setsid() == -1)
                return (-1);

        if (nochdir == 0) {
                if(chdir("/") != 0) {
                        perror("chdir");
                        return (-1);
                }
        }

        if (noclose == 0 && (fd = open("/dev/null", O_RDWR, 0)) != -1) {
                if(dup2(fd, STDIN_FILENO) < 0) {
                        perror("dup2 stdin");
                        return (-1);
                }
                if(dup2(fd, STDOUT_FILENO) < 0) {
                        perror("dup2 stdout");
                        return (-1);
                }
                if(dup2(fd, STDERR_FILENO) < 0) {
                        perror("dup2 stderr");
                        return (-1);
                }

                if (fd > STDERR_FILENO) {
                        if(close(fd) < 0) {
                                perror("close");
                                return (-1);
                        }
                }
        }
        return (0);
}

int is_root()
{
        if (getuid() == 0 || geteuid() == 0)
                return 1;

        return 0;
}

int set_proc_user(char * username)
{
        if (*username == '\0') {
            fprintf(stderr, "username is invalid!\n");
            exit(1);
        }

        if ((pw = getpwnam(username)) == 0) {
            fprintf(stderr, "can't find the user %s to switch to\n", username);
            exit(1);
        }

        if (setgid(pw->pw_gid) < 0 || setuid(pw->pw_uid) < 0) {
            fprintf(stderr, "failed to assume identity of user %s\n", username);
            exit(1);
        }

}
